import React,{Component} from 'react';
import ReactTooltip from 'react-tooltip';


export class Cash extends Component {
	constructor(props){
		super(props);
		this.state = {
			value : '',
			total : 500,
			clicks : 0
		};
	}

	render(){
		var percentage = Number(localStorage.getItem('totalCash')) + 'px';
		var needed = '$' + (Number(this.state.total) - Number(localStorage.getItem('totalCash'))) + ' still needed for this project';
		var marginTop = '25px';
		var xy = this.state.value ? 'Why give $' + this.state.value + '?': '';
		return (

			<div>
				
				<div className="box1">

					<div className="progress" data-tip={needed}>
					  <div id="bg-orange" className="progress-bar bg-warning" role="progressbar" style={{width: percentage}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="500"></div>
					  <ReactTooltip />
					</div>
				</div>
				<div className="box">
					

					<h5 style={{marginTop}}><span style={{color: '#F76300'}}>Only 3 days left</span> to fund this project.</h5>

					<h5 style={{marginTop}}>Join the { localStorage.getItem('totalClick') } other donors who have already supported this project. Every dollar helps.</h5>

					<div className="buttonContent">
						<input id="cash" type="text" name="cash" placeholder="$" onChange ={evt => this.setCash(evt)}/>
						<button className="button1 btn btn-success" onClick={ evt => this.payCash()}>Give Now</button>
					</div>
					<div className="box3">
						<a href="">{xy}</a>
					</div>
				</div>
				<div className="box2">
					<button className="button2 btn btn-light">Save for later</button>
					<button className="button2 btn btn-light">Tell your friends</button>
				</div>

			</div>
			
			
		)
	}

	setCash(evt) {
		this.setState({
			value : evt.target.value
		});
	}

	payCash() {

		if(this.state.value != ''){
			if (localStorage.getItem('totalCash') == null || localStorage.getItem('totalCash') == 0) {
				localStorage.setItem('totalCash', this.state.value);
			} else {
				const total = Number(localStorage.getItem('totalCash')) + Number(this.state.value);
				localStorage.setItem('totalCash', total); 
			}

			if (localStorage.getItem('totalClick') == null || localStorage.getItem('totalClick') == 0) {
				this.setState({
					clicks : Number(1)
				});
				localStorage.setItem('totalClick', Number(1));
			} else {
				let a = localStorage.getItem('totalClick');
				let t = Number(a) + 1;

				this.setState({
					clicks : Number(t)
				});

				localStorage.setItem('totalClick', t);
			}
		}

		
	}

	clearCash() {
		
		localStorage.setItem('totalCash', Number(0));
		localStorage.setItem('totalClick', Number(0));
	}

	checkCash() {
		console.log(localStorage.getItem('totalCash'));
		console.log(localStorage.getItem('totalClick'));
	}
}