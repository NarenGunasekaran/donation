import logo from './logo.svg';
import './App.css';
import {Cash} from './components/Cash';
import Progress from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
    return ( 
    	<div className = "App" >
    		<div className="content">
				<Cash/>
    		</div>
        </div>
    );
}

export default App;